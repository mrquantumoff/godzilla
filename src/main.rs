mod pkg;
use colored::*;
use nix::unistd::Uid;
fn main() {
    if !Uid::effective().is_root() {
        println!(
            "{}",
            "Godzilla package manager must be started as root!".bright_red()
        );
        std::process::exit(1);
    }
    let pkgver = pkg::VersionNormal::new(1, 0, 0, 0, 1);
    let packager: pkg::Packager = pkg::Packager {
        name: "Bultek.".to_string(),
        email: "help@bultek.com.ua".to_string(),
    };
    let _pkg = pkg::Package::new(
        "godzilla".to_string(),
        "Godzilla package manager".to_string(),
        pkg::VersionType::VersionNormal(pkgver),
        pkg::PKG::Nothing,
        pkg::PKG::Nothing,
        pkg::PKG::Nothing,
        "".to_string(),
        600,
        pkg::Arch::x86_64,
        packager,
    );
    match _pkg.version {
        pkg::VersionType::VersionNormal(ver) => {
            let r = pkg::VersionNormal::render(&ver);
            println!("{}", r)
        }

        _ => {}
    }
}
