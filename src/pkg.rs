#![allow(dead_code)]
#![allow(improper_ctypes_definitions)]
#![allow(unreachable_code)]

extern crate tar;
use colored::*;
use core::result::Result;
use std::env;
use std::fs;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use tar::Archive;

#[repr(C)]
pub struct Package {
    pub name: String,
    pub description: String,
    pub version: VersionType,
    pub depends: PKG,
    pub replaces: PKG,
    pub conflicts: PKG,
    pub url: String,
    pub size: i64,
    pub arch: Arch,
    pub packager: Packager,
}

#[repr(C)]
pub struct Packager {
    pub name: String,
    pub email: String,
}

#[repr(C)]
#[allow(non_camel_case_types)]
pub enum Arch {
    x86_64,
    i686,
    i586,
    aarch64,
    any,
}

#[repr(C)]
pub enum PKG {
    Package(Box<String>),
    Nothing,
    Packages(Vec<String>),
}

#[repr(C)]
pub enum VersionType {
    VersionNeutral,
    VersionNormal(VersionNormal),
    VersionVCS,
}

#[repr(C)]
pub struct VersionNormal {
    pub major: i32,
    pub minor: i32,
    pub patch: i32,
    pub epoch: i32,
    pub release: i32,
}

pub enum ErrorMessage {
    Message(String),
}

impl Package {
    #[no_mangle]
    pub extern "C" fn new(
        name: String,
        desc: String,
        version: VersionType,
        depends: PKG,
        replaces: PKG,
        conflicts: PKG,
        url: String,
        size: i64,
        arch: Arch,
        packager: Packager,
    ) -> Self {
        Self {
            name: name,
            description: desc,
            version: version,
            depends: depends,
            replaces: replaces,
            conflicts: conflicts,
            url: url,
            size: size,
            arch: arch,
            packager: packager,
        }
    }

    #[no_mangle]
    /// Reads content from an archlinux package and returns info about it
    pub extern "C" fn read_from_file(path: PathBuf) {
        // Untar the file
        let filename = &path
            .file_name()
            .unwrap()
            .to_owned()
            .to_str()
            .unwrap()
            .to_owned();
        // Create dir /var/cache/godzilla/unpacked/filename
        let dirname = PathBuf::from(String::from(
            "/var/cache/godzilla/unpacked/".to_owned() + filename,
        ));
        // Check if the dir already exists
        if fs::metadata(&dirname).is_ok() {
            fs::remove_dir(&dirname).unwrap_or(panic!("Error while removing useless dir"));
        }
        fs::create_dir_all(&dirname).unwrap_or(panic!("Error while creating cache dir"));
        let out = unpack(&path, &dirname);
        match out {
            Ok(o) => {
                let _prevdir = env::current_dir().unwrap_or(PathBuf::from("~"));
                env::set_current_dir(&o)
                    .unwrap_or(panic!("Error while interacting with the environment"));
                let _f = File::open(".PKGINFO").unwrap_or(panic!("Error while opening .PKGINFO"));
                let mut conts = String::new();
                let r = _f.read_to_string(&mut conts);
                match r {
                    Ok(_) => {}
                    Err(_) => {
                        println!("{}", "Error while reading .PKGINFO".bright_red());
                        std::process::exit(1);
                    }
                }
                let mut pkgver = String::new();
                let mut deps: Vec<String> = Vec::new();
                let mut replaces: Vec<String> = Vec::new();
                let mut conflicts: Vec<String> = Vec::new();
                let mut desc = String::new();
                let mut arch = String::new();
                let mut size = String::new();
                let mut pkger = String::new();
                let mut url: String = String::new();
                let lines: Vec<&str> = conts.split("\n").collect();
                for line in lines {
                    let line = line.to_string();
                    if line == "#" {
                        continue;
                    }
                    let words: Vec<&str> = line.split(" ").collect();
                    let mut wrd = keys::none;
                    let mut prwrd = keys::none;
                    for word in words {
                        if wrd == keys::shldskip {
                            continue;
                        }
                        match word {
                            "depend" => {
                                wrd = keys::shldskip;
                                prwrd = keys::dep;
                            }
                            "optdepend" => {
                                wrd = keys::shldskip;
                                prwrd = keys::dep;
                            }
                            "makedepend" => {
                                wrd = keys::shldskip;
                                prwrd = keys::dep;
                            }
                            "size" => {
                                wrd = keys::shldskip;
                                prwrd = keys::size;
                            }
                            "replaces" => {
                                wrd = keys::shldskip;
                                prwrd = keys::replaces;
                            }
                            "conflicts" => {
                                wrd = keys::shldskip;
                                prwrd = keys::conflicts;
                            }
                            "url" => {
                                wrd = keys::shldskip;
                                prwrd = keys::url;
                            }
                            "pkgdesc" => {
                                wrd = keys::shldskip;
                                prwrd = keys::desc;
                            }
                            "packager" => {
                                wrd = keys::none;
                                prwrd = keys::pkger;
                            }
                            "version" => {
                                wrd = keys::shldskip;
                                prwrd = keys::ver;
                            }
                            &_ => match prwrd {
                                keys::dep => {
                                    deps.push(word.to_string());
                                    wrd = keys::none
                                }
                                keys::size => {
                                    size = word.to_string();
                                    wrd = keys::none
                                }
                                keys::arch => {
                                    arch = word.to_string();
                                    wrd = keys::none
                                }
                                keys::replaces => {
                                    replaces.push(word.to_string());
                                    wrd = keys::none
                                }
                                keys::conflicts => {
                                    conflicts.push(word.to_string());
                                    wrd = keys::none
                                }
                                keys::ver => {
                                    pkgver = word.to_string();
                                    wrd = keys::none;
                                }
                                keys::url => {
                                    url = word.to_string();
                                    wrd = keys::none;
                                }
                                keys::desc => {
                                    desc = word.to_string();
                                    wrd = keys::none;
                                }
                                keys::pkger => {
                                    pkger = word.to_string();
                                }
                                _ => {}
                            },
                        }
                    }
                }
            }
            Err(e) => {
                println!("{}", e.bright_red());
                std::process::exit(1);
            }
        }
    }
}

#[derive(PartialEq)]
#[allow(non_camel_case_types)]
enum keys {
    shldskip,
    conflicts,
    replaces,
    none,
    dep,
    name,
    ver,
    desc,
    url,
    size,
    pkger,
    arch,
}

#[no_mangle]
pub fn unpack(file: &PathBuf, _to: &PathBuf) -> Result<PathBuf, String> {
    let _file = File::open(file).unwrap_or(panic!("Error while opening an archive"));
    let arc = Archive::new(_file);
    arc.set_unpack_xattrs(true);
    arc.set_preserve_permissions(true);
    let res = arc.unpack(_to);
    match res {
        Ok(()) => Ok(_to.to_owned()),
        Err(_) => Err("Failed to unpack".to_string()),
    }
}

impl VersionNormal {
    #[no_mangle]
    pub extern "C" fn new(major: i32, minor: i32, patch: i32, epoch: i32, release: i32) -> Self {
        VersionNormal {
            major: major,
            minor: minor,
            patch: patch,
            epoch: epoch,
            release: release,
        }
    }

    #[no_mangle]
    pub extern "C" fn from_string(mut input: String) -> VersionType {
        let mut major: i32;
        let mut minor: i32;
        let mut patch: i32;
        let mut epoch: i32;
        let mut release: i32;

        let basicsplit: Vec<&str> = input.split(".").collect();
        let relsplit: Vec<&str> = basicsplit[basicsplit.len()-1].split("-").collect();
        let epochsplit: Vec<&str> = basicsplit[0].split(":").collect();
        epoch = relsplit[0].parse().unwrap_or(0);
        release = relsplit[1].parse().unwrap_or(0);
        let inc: Vec<char> = input.chars().collect();
        for c in &inc {
            let c = c.to_owned();
            if c == '-' {
                // Get position of an item in vector
                let index = index_of_char(&inc, &c);
                input.remove(index);
                input.remove(index-1);
            }
            else if c == '+' {
                let index = index_of_char(&inc, &c);
                input.remove(index);
            }
            else if c == ':' {
                let index = index_of_char(&inc, &c);
                input.remove(index);
                input.remove(index+1);  
            }

        }
        todo!()
    }

    #[no_mangle]
    pub extern "C" fn render(ver: &Self) -> String {
        String::from(
            ver.epoch.to_string().as_str().to_owned()
                + ":"
                + ver.major.to_string().as_str()
                + "."
                + ver.minor.to_string().as_str()
                + "."
                + ver.patch.to_string().as_str()
                + "-"
                + ver.release.to_string().as_str(),
        )
    }
}
fn index_of_char(input1: &Vec<char>, input2: &char) -> usize {
    input1.into_iter()
   .position(|x| x == input2)
   .unwrap_or(0)
}